<!DOCTYPE>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>ArmoCCF</title>
		<link type ="text/css" rel="stylesheet" media="all" href="style.css">
	</head>
	<body>
		<div class="info">
			<form class="deco" action="index.html">
					<input type="submit" value="Déconnexion">
			</form>
			<?php
				try
				{
					$bdd = new PDO('mysql:host=localhost;dbname=ppe;charset=utf8', 'root', '');
				}
				catch(Exception $erreur)
				{
					die('Erreur : '.$erreur->getMessage());
				}
					$requeteSQL = "SELECT * FROM utilisateur";
					$reponse = $bdd->query($requeteSQL);
					while ($donnees = $reponse->fetch())
					{
						if($_GET["log"] == $donnees["login"] && $_GET["pwd"] == $donnees["mdp"])
						{
								break;
						}
					}
				echo '
				Etablissement: '.$donnees["code_etab"].'<br>
				Session: '.$donnees["annee"].'<br>';
			?>
		</div>
		<div class="logo">
		
			<img class="lg" src="logo.png">
			<p>
			<?php
				
				echo" <table style=\"text-align:center\">
				<caption>Informations sur la session</caption>
				
					<tr>
						<th>
							Section
						</th>
						<th>
							Série
						</th>
						<th>
							Epreuve
						</th>
						<th>
							Nombre de candidats notés
						</th>
						<th>
							Nom du responsable de l'épreuve
						</th>
						<th>
							Mail du responsable
						</th>
						<th>
							Téléphone du responsable
						</th>
						<th>
							Action
						</th>
					
					
					</tr>";
				$tabserie;
				$i = 0;
				
					$requeteSQL = "SELECT * FROM utilisateur";
					$reponse = $bdd->query($requeteSQL);
					while ($donnees = $reponse->fetch())
					{
						if($_GET["log"] == $donnees["login"] && $_GET["pwd"] == $donnees["mdp"])
						{
								break;
						}
					}
					$requeteSQL = "SELECT * FROM section";
					$reponse = $bdd->query($requeteSQL);
					while ($section = $reponse->fetch())
					{
						if($section["numEtab"] == $donnees["code_etab"])
						{
							$tabserie[$i]  = $section["idSerie"];
							$i++;	
						}
					}
					foreach($tabserie as $val)
					{
						$requeteSQL = "SELECT * FROM epreuve";
						$reponse = $bdd->query($requeteSQL);
						while ($epreuve = $reponse->fetch())
						{
							if($epreuve["idSerie"] == $val)
							{
								$requeteSQL = "SELECT * FROM section";
								$reponse_section = $bdd->query($requeteSQL);
								while ($section = $reponse_section->fetch())
								{
									if($section["numEtab"] == $donnees["code_etab"] && $val == $section["idSerie"])
									{
										break;	
									}
								}
								echo '
									<tr>
										<form method="POST" action="participer.php">
											<td>
												<input type="hidden" name="numSection" value="'.$section["numSection"].'">'.$section["nomSection"].'
											</td>
											<td>
												<input type="hidden" name="annee" value="'.$donnees["annee"].'">'.$epreuve["idSerie"].'
											</td>
											<td>
												<input type="hidden" name="codeEpreuve" value="'.$epreuve["codeEpreuve"].'">'.$epreuve["nomEpreuve"].'
											</td>
											<td>
												<input type="text" name="nb" placeholder = "Nombre de participant">
											</td>
											<td>
												<input type="text" name="nomResp" placeholder = "Nom du responsable">
											</td>
											<td>
												<input type="text" name="mailResp" placeholder = "Adresse mail du responsable">
											</td>
											<td>
												<input type="text" name="telResp" placeholder = "Numero de téléphone du Responsable">
											</td>
											<td>
												<input type="submit" name="action" value="envoyer">
											</td>
										</form>
									</tr>';
									$i++;
							}
						}
					}
						
			?>
			</table>
			
			</p>
			<div class="tableau">
			<br>
			</br>
			<br>
			</br>
			<br>
			</br>
			<table style="text-align:center">
					<caption>Récapitulatifs sur la session 2017</caption>
						<tr>
							<th>
								Section
							</th>
							<th>
								Session
							</th>
							<th>
								Epreuve
							</th>
							<th>
								nb candidats
							</th>
							<th>
								moyenne
							</th>
							<th>
								minimum
							</th>
							<th>
								maximum
							</th>
							<th>
								note en dessous de 8
							</th>
							<th>
								note entre 8 à 10
							</th>
							<th>
								note entre 10 à 12
							</th>
							<th>
								note au dessus de 12
							</th>
							<th>
								ecart type
							</th>
						</tr>
						<?php
						$requeteSQL = "SELECT * FROM restituer";
						$reponse = $bdd->query($requeteSQL);
						while ($restituer = $reponse->fetch())
						{
							$requeteSQL = "SELECT * FROM participer";
							$reponse_part = $bdd->query($requeteSQL);
							while ($part = $reponse_part->fetch())
							{
								if($part["codeEpreuve"] == $restituer["codeEpreuve"])
								{
										break;
								}
							}
							echo '<tr>
							<td>
								'.$restituer["numSection"].'
							</td>
							<td>
								'.$restituer["annee"].'
							</td>
							<td>
								'.$restituer["codeEpreuve"].'
							</td>
							<td>
								'.$part["nbParticipant"].'
							</td>
							<td>
								'.$restituer["Moy"].'
							</td>
							<td>
								'.$restituer["Min"].'
							</td>
							<td>
								'.$restituer["Max"].'
							</td>
							<td>
								'.$restituer["inf8"].'
							</td>
							<td>
								'.$restituer["inf10"].'
							</td>
							<td>
								'.$restituer["sup10"].'
							</td>
							<td>
								'.$restituer["sup12"].'
							</td>
							<td>
								'.$restituer["et"].'
							</td>
						</tr>';
						}
						?>
					</table>
			</div>
		</div>
	</body>
</html>
