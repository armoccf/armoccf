<!DOCTYPE>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>ArmoCCF</title>
		<link type ="text/css" rel="stylesheet" media="all" href="style.css">
	</head>
	<body>
		<div class="info">
				<form class="deco" action="index.html">
					<input type="submit" value="Déconnexion">
				</form>
				<?php
				try
				{
					$bdd = new PDO('mysql:host=localhost;dbname=ppe;charset=utf8', 'root', '');
				}
				catch(Exception $erreur)
				{
					die('Erreur : '.$erreur->getMessage());
				}
		
				$requeteSQL = "SELECT login, mdp, codeEpreuve, code_etab, numSection, annee, role, debut, fin FROM Utilisateur";
				$reponse = $bdd->query($requeteSQL);
		
				while ($donnees = $reponse->fetch())
				{
					if($donnees["login"] == $_POST["log"])
					{
						echo '
						Epreuve: '.$donnees["codeEpreuve"].'<br>
						Etablissement: '.$donnees["code_etab"].'<br>
						Session: '.$donnees["annee"].'<br>
						';
						break;
					}
				}
			echo '</div>
		<div class="logo">
			<img class="lg" src="logo.png"><br>
				<center>';
						$moy =  0;
						$tab =  $_POST["nom"];
						$i8  =  0;
						$i10 =  0;
						$i12 =  0;
						$s12 =  0;
						$min =  21;
						$max =  -1;
			
						for($i=0;$i<$_POST["nb"];$i++)
						{
							echo 1+$i.": ".$tab[$i].'<br>';
							$moy = $moy + $tab[$i];
					
							if ($tab[$i]<=8) {
							$i8=$i8+1;
						}
						if ($tab[$i]>8 && $tab[$i]<=10) {
							$i10=$i10+1;
						}
						if ($tab[$i]>10 && $tab[$i]<=12) {
							$i12=$i12+1;
						}
						if ($tab[$i]>12) {
							$s12=$s12+1;
						}
						if($tab[$i] > $max)
						{
							$max = $tab[$i];
						}
						if($tab[$i] < $min)
						{
							$min = $tab[$i];
						}
					}
					$moy = $moy / $_POST["nb"];
	
					$var = 0;
					$val = 0;
			
					foreach($tab as $val)
					{
						$var = $var + ($val-$moy)*($val-$moy);
					}
					$var = $var/($_POST["nb"]-1);
					$et = sqrt($var);

					echo '
						
						min: '.$min.'
						max: '.$max.'
						moy: '.$moy.'
						inf8: '.$i8.'
						inf10: '.$i10.'
						sup10: '.$i12.'
						sup12: '.$s12.'
						et: '.$et.'
						
						';
				?>
				<form method="POST" class="val" action="restituer.php">
					<input type="hidden" name="et" value="<?php echo $et?>">
					<input type="hidden" name="nb" value="<?php echo $_POST["nb"]?>">
					<input type="hidden" name="numSection" value="<?php echo $donnees["numSection"]?>">
					<input type="hidden" name="annee" value="<?php echo $donnees["annee"]?>">
					<input type="hidden" name="codeEpreuve" value="<?php echo $donnees["codeEpreuve"]?>">
					<input type="hidden" name="moy" value="<?php echo $moy?>">
					<input type="hidden" name="inf8" value="<?php echo $i8?>">
					<input type="hidden" name="inf10" value="<?php echo $i10?>">
					<input type="hidden" name="sup10" value="<?php echo $i12?>">
					<input type="hidden" name="sup12" value="<?php echo $s12?>">
					<input type="hidden" name="max" value="<?php echo $max?>">
					<input type="hidden" name="min" value="<?php echo $min?>">
					<input type="submit" name="valide" value="OK">
				</form>
				<form class="ref" action="entrer.php">
					<input type="submit" name="ref" value="retour">
				</form>
				</center>
			
			<div class="tableau">
			</div>
		</div>
	</body>
</html>
