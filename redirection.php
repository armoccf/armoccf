<?php
	$log = 0;
	try
	{
		$bdd = new PDO('mysql:host=localhost;dbname=ppe;charset=utf8', 'root', '');
	}
	catch(Exception $erreur)
	{
		die('Erreur : '.$erreur->getMessage());
	}
	
	$requeteSQL = "SELECT login, mdp, codeEpreuve, code_etab, numSection, annee, role, debut, fin FROM Utilisateur";
	$reponse = $bdd->query($requeteSQL);
	
	while ($donnees = $reponse->fetch())
	{
		if($donnees["login"] == $_POST["log"] && $donnees["mdp"] == $_POST["pwd"])
		{
			$log = 1;
			break;
		}
	}
	
	if($donnees["role"] == "prof")
	{
		header('Location: entrer.php?log='.$_POST["log"].'&pwd='.$_POST["pwd"]);
	}
	if($donnees["role"] == "etab")
	{
		header('Location: final.php?log='.$_POST["log"].'&pwd='.$_POST["pwd"]);
	}
	
	if($log != 1)
	{
		header('Location: index.html');
	}
	
?>